# ccollections

This project is a set of generic collections implemented in C. These are:

- List.
	- Stack.
	- Queue.
	- String.
- Unique list.
- Set.
- Multiset.
- Vector.
- Map.
- Multimap.
- Tree.
- Graph.

## Purpose

> Show **how to implement** generic collections in C.

## Project Structure

- `src/`: all source code.
	- `list/`: list module.
	- `stack/`: stack module.
	- ...
- `bin/`: all object files.
- `test/`: all unit tests.
- `doc/`: analysis and design of the project.

Each module consists of 2 files for each ADT:

- Header file.
- Source file.

## Compile

```bash
> make
```
