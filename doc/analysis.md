# Analysis

This document explains **what** this project does.

# Interface
## Types

There are several classes of collections.

### Variable-size

The definition of a variable-size collection has three variables **(Syntactic order, Semantic order, Repeat),** all boolean. Depending on their values we can classify the collections.

| **Syntactic order** | **Semantic order** | **Repeat** | **Collection** |
| :-----------------: | :----------------: | :--------: | :------------- |
| 1 | 1 | 1 | **Ordered multiset** |
| 1 | 1 | 0 | **Ordered set**      |
| 1 | 0 | 1 | **List**             |
| 1 | 0 | 0 | **Unique list**      |
| 0 | 1 | 1 | -                    |
| 0 | 1 | 0 | -                    |
| 0 | 0 | 1 | **Multiset**         |
| 0 | 0 | 0 | **Set**              |

There are two more collections which can be based on:

- **List:**
	- Stack.
	- Queue.
- **Ordered multiset:**
	- Ordered stack.
	- Ordered queue.

### Constant-size

The only two collections that have constant-size are tuple and vector. Both syntactically ordered, not semantically ordered and allow repeated values. The only difference is:

- **Tuple:** elements of different type.
- **Vector:** elements of same type.

**Note:** languages provide a built-in tuple interface, these may be called structure, record, class, algebraic data (sums of product or discriminated union of tuples).

### Relation-serialization

Relations (or functions) can be stored in collections named map. A map associates a unique key a value. The next figure shows the most general map:

![Mulitkey multimap](img/multikeymultimap.png)

The most general mapping of a relation is from "n-tuple - m-tuple". Though the most used is from "1-tuple - 1-tuple", that is, a map. Depending on the generity/specifity of the relation we may have:

- **Multikey multimap:** "n-tuple - m-tuple".
- **Multikey map:** "n-tuple - 1-tuple".
- **Multimap:** "1-tuple - n-tuple".
- **Map:** "1-tuple - 1-tuple".

### Tree

**To Do.**

### Graph

**To Do.**

## Functions

- **List:**
	- Add by index.
	- Get by index.
	- Replace by index.
	- Remove by index.
	- Concatenate.
	- Sort.
- **Set:**
	- Insert.
	- Delete.
	- Union.
	- Intersection.
	- Difference.
	- Power set.
- **Vector:**
	- Set by index.
	- Get by index.
- **Map:**
	- Put.
	- Look-up.
	- Delete.
	- Union.
	- Intersection.
	- Difference.
- **Graph:**
- **Tree:**
