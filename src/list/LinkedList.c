#include "LinkedList.h"

struct LinkedList {
	T item;
	LinkedList* tail;
};

LinkedList* linkedlist_new() {
	// Variables
	LinkedList* l;
	
	// Algorithm
	l = malloc(sizeof(LinkedList));
	if (l == NULL) return NULL;
	
	l->tail = NULL;

	return l;
}

void linkedlist_free(LinkedList* l) {
	// Preconditions
	assert(l != NULL);

	// Base case
	if (l->tail == NULL) {
		free(l);
		return ;
	}
	
	// Variables
	LinkedList* tail;
	
	// Algorithm
	tail = l->tail;
	free(l);
	linkedlist_free(tail);
}

void linkedlist_add(LinkedList* l, Integer i, T x) {
	// Preconditions
	assert(l != NULL);
	assert(i >= 0 && i <= linkedlist_size(l));

	// Algorithm
	linkedlist_recursive_add(l, i ,x);
}

static void linkedlist_recursive_add(LinkedList* l, Integer i, T x) {
	// Base case
	if (i == 0) {
		linkedlist_prepend(l, x);
		return ;
	}

	linkedlist_recursive_add(l->tail, i - 1, x);
}

void linkedlist_prepend(LinkedList* l, T x) {
	// Preconditions
	assert(l != NULL);
	
	// Variables
	LinkedList* newnode;
	
	// Algorithm
	newnode = malloc(sizeof(LinkedList));
	if (newnode == NULL) {
		l->error = CC_MALLOC_ERROR;
		return ;
	}

	newnode->item = x;
	newnode->tail = l->tail;
	l->tail = newnode;
}

void linkedlist_append(LinkedList* l, T x) {
	// Preconditions
	assert(l != NULL);

	linkedlist_recursive_append(l, x);
}

void linkedlist_recursive_append(LinkedList* l, T x) {
	// Base case
	if (l->tail == NULL) {
		linkedlist_prepend(l, x);
		return ;
	}

	linkedlist_recursive_append(l->tail, x);
}

T linkedlist_get(LinkedList* l, Integer i);
T linkedlist_head(LinkedList* l);
T linkedlist_last(LinkedList* l);

T linkedlist_replace_x(LinkedList* l, T x, T y);
T linkedlist_replace(LinkedList* l, Integer i, T x);
T linkedlist_replace_head(LinkedList* l, T x);
T linkedlist_replace_last(LinkedList* l, T x);

T linkedlist_remove_x(LinkedList* l, T x);
T linkedlist_remove(LinkedList* l, Integer i);
T linkedlist_remove_head(LinkedList* l);
T linkedlist_remove_last(LinkedList* l);

void        linkedlist_concatenate(LinkedList* l1, LinkedList* l2);
LinkedList* linkedlist_init(LinkedList* l);
LinkedList* linkedlist_tail(LinkedList* l);
void        linkedlist_sort(LinkedList* l1, FunctionCompare f);
int         linkedlist_null(LinkedList* l);

Integer linkedlist_size(LinkedList* l);
int     linkedlist_in(LinkedList* l, T x);
