#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ArrayList.h"

#define DEFAULT_SIZE 4

struct ArrayList {
	T* items;
	Integer n;
	Integer size;
	Error error;
};

ArrayList* arraylist_new() {
	// Variables
	ArrayList* l;

	// Algorithm
	l = malloc(sizeof(ArrayList));
	if (l == NULL) return NULL;
	
	l->n = 0;
	l->size = DEFAULT_SIZE;
	l->items = malloc(n*sizeof(T));
	if (l->items == NULL) l->error = CC_MALLOC_ERROR;
	else l->error = CC_SUCCESS;

	return l;
}

void arraylist_free(ArrayList* l) {
	// Preconditions
	assert(l != NULL);
	assert(arraylist_null(l));

	// Algorithm
	free(l->items);
	free(l);
}

void arraylist_add(ArrayList* l, Integer i, T item) {
	// Preconditions
	assert(l != NULL);
	assert(i >= 0 && i <= l->n);
	
	// Variables
	void* src;
	void* dst;
	Integer size;

	// Algorithm
	// Move data
	src = l->items + i;
	dst = l->items + i + 1;
	size = l->n - i;
	memmove(dst, src, size);
	
	// Update structure
	l->items[i] = T;
	l->n++;
}

void arraylist_prepend(ArrayList* l, T item) {
	arraylist_add(l, 0, item);
}

void arraylist_append(ArrayList* l, T item) {
	// TODO Check if compiler optimizes `add` (does not call memmove)
	arraylist_add(l, l->n, item);
}

T arraylist_get(ArrayList* l, Integer i) {
	// Preconditions
	assert(l != NULL);
	assert(i >= 0 && i < l->n);

	// Algorithm
	return l->items[i];
}

T arraylist_head(ArrayList* l) {
	return arraylist_get(l, 0);
}

T arraylist_last(ArrayList* l) {
	return arraylist_get(l, l->n - 1);
}

T arraylist_replace_x(ArrayList* l, T x, FunctionCompare f, T y) {
	// Preconditions
	assert(l != NULL);
	assert(!arraylist_null(l));
	assert(f != NULL);
	assert(arraylist_in(l, x ,f));

	// Variables
	T oldx;

	// Algorithm
	for (Integer i = 0; i < l->n; i++) {
		if (f(l->items[i], x) == 0) {
			// Get old item
			oldx = l->items[i];
			// Set old item
			l->items[i] = y;
		}
	}

	return oldx;
}

T arraylist_replace(ArrayList* l, Integer i, T x) {
	// Preconditions
	assert(l != NULL);
	assert(i >= 0 && i < l->n);

	// Variables
	T oldx;

	// Algorithm
	oldx = l->item[i];
	l->item[i] = item;
	
	return oldx;
}

T arraylist_replace_head(ArrayList* l, T x) {
	return arraylist_replace(l, 0, x);
}

T arraylist_replace_last(ArrayList* l, T x) {
	return arraylist_replace(l, l->n - 1, x);
	
}

T arraylist_remove_x(ArrayList* l, FunctionCompare f, T x) {
	
}

T arraylist_remove(ArrayList* l, Integer i) {
	// Preconditions
	assert(l != NULL);
	assert(i >= 0 && i < l->n);
	
	// Variables
	T item;
	void* src;
	void* dst;
	Integer size;

	// Algorithm
	// Retrieve item
	item = l->items[i];

	// Move data
	src = l->items + i + 1;
	dst = l->items + i;
	size = l->n - i + 1;
	memmove(src, dst, size);
	
	// Update structure
	l->n--;

	return item;
}

T arraylist_remove_head(ArrayList* l) {
	return arraylist_remove(l, 0);
}

T arraylist_remove_last(ArrayList* l) {
	// TODO Check if compiler optimizes
	return arraylist_remove(l, l->n - 1);
}

void arraylist_concatenate(ArrayList* l1, ArrayList* l2) {
	// Preconditions
	assert(l1 == NULL);
	assert(l2 == NULL);
	
	// Variables
	int size;

	// Algorithm
	// Resize list
	size = l1->size + l2->size;
	arraylist_resize(l1, size);
	if (l1->error == CC_MALLOC_ERROR) return ;
	
	// Append data
}

ArrayList* arraylist_init(ArrayList* l) {
	
}

ArrayList* arraylist_tail(ArrayList* l) {
	
}

void arraylist_sort(ArrayList* l, FunctionCompare f) {

}

int arraylist_null(ArrayList* l) {
	// Preconditions
	assert(l != NULL);
	
	// Algorithm
	return l->n == 0;
}

Integer arraylist_size(ArrayList* l) {
	// Preconditions
	assert(l != NULL);
	
	// Algorithm
	return l->n;
}

int arraylist_in(ArrayList* l, T item, FunctionCompare f) {
	// Preconditions
	assert(l != NULL);
	assert(!arraylist_null(l));
	assert(f != NULL);
	
	// Algorithm
	for (Integer i = 0; i < l->n; i++) {
		if (f(l->items[i], item) == 0) return 1;
	}

	return 0;
}
