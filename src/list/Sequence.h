#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "General.h"

// Types
typedef struct Sequence Sequence;

// Functions
// MEMORY ALLOCATION
Sequence * sequence_new();
void       sequence_free(Sequence * l);

Sequence * sequence_copy(Sequence * l);
Sequence * sequence_deep_copy(Sequence * l, FunctionCopy f);

// CONTENT
void sequence_prepend(Sequence * l, T item);

T sequence_get(Sequence * l, Integer i);
T sequence_head(Sequence * l);
T sequence_tail(Sequence * l);

// QUERY STATE
Integer sequence_size(Sequence * l);
int     sequence_is_empty(Sequence * l);

// SPECIFIC
sequence_concatenate(Sequence * l1, Sequence * l2);
sequence_sort(Sequence * l);

#endif
