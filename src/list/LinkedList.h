#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "General.h"

// Types
typedef struct LinkedList LinkedList;

// Functions
/// Memory allocation
LinkedList* linkedlist_new();
void        linkedlist_free(LinkedList* l);

/// Content
void linkedlist_add(LinkedList* l, Integer i, T x);
void linkedlist_prepend(LinkedList* l, T x);
void linkedlist_append(LinkedList* l, T x);

T linkedlist_get(LinkedList* l, Integer i);
T linkedlist_head(LinkedList* l);
T linkedlist_last(LinkedList* l);

T linkedlist_replace_x(LinkedList* l, T x, T y);
T linkedlist_replace(LinkedList* l, Integer i, T x);
T linkedlist_replace_head(LinkedList* l, T x);
T linkedlist_replace_last(LinkedList* l, T x);

T linkedlist_remove_x(LinkedList* l, T x);
T linkedlist_remove(LinkedList* l, Integer i);
T linkedlist_remove_head(LinkedList* l);
T linkedlist_remove_last(LinkedList* l);

/// Specific
void        linkedlist_concatenate(LinkedList* l1, LinkedList* l2);
LinkedList* linkedlist_init(LinkedList* l);
LinkedList* linkedlist_tail(LinkedList* l);
void        linkedlist_sort(LinkedList* l1, FunctionCompare f);
int         linkedlist_null(LinkedList* l);

/// Generic
Integer linkedlist_size(LinkedList* l);
int     linkedlist_in(LinkedList* l, T x);

#endif
