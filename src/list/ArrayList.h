#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H

#include "General.h"

// Types
typedef struct ArrayList ArrayList;

// Functions
/// Memory allocation
ArrayList* arraylist_new();
void       arraylist_free(ArrayList* l);

/// Content
void arraylist_add(ArrayList* l, Integer i, T x);
void arraylist_prepend(ArrayList* l, T x);
void arraylist_append(ArrayList* l, T x);

T arraylist_get(ArrayList* l, Integer i);
T arraylist_head(ArrayList* l);
T arraylist_last(ArrayList* l);

T arraylist_replace_x(ArrayList* l, T x, FunctionCompare f, T y);
T arraylist_replace(ArrayList* l, Integer i, T x);
T arraylist_replace_head(ArrayList* l, T x);
T arraylist_replace_last(ArrayList* l, T x);

T arraylist_remove_x(ArrayList* l, FunctionCompare f, T x);
T arraylist_remove(ArrayList* l, Integer i);
T arraylist_remove_head(ArrayList* l);
T arraylist_remove_last(ArrayList* l);

/// Specific
void       arraylist_concatenate(ArrayList* l1, ArrayList* l2);
ArrayList* arraylist_init(ArrayList* l);
ArrayList* arraylist_tail(ArrayList* l);
void       arraylist_sort(ArrayList* l1, FunctionCompare f);
int        arraylist_null(ArrayList* l);

/// Generic
Integer arraylist_size(ArrayList* l);
int     arraylist_in(ArrayList* l, T x);

#endif
