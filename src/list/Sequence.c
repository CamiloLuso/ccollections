#include "Sequence.h"

struct Sequence {
	Node * head;
};

struct Node {
	T item;
	Node * next;
};

Sequence * sequence_new() {
	// Variables
	Sequence * s;

	// Algorithm
	s = malloc(sizeof(struct Sequence));
	return s;
}

void sequence_free(Sequence * s) {
	// Preconditions
	assert(l != NULL);
	
	// Variables
	Node * next;

	// Algorithm
	next = s->head->next;
	free(s->head);
	s->head = next;
	sequence_free(s);
}

Sequence * sequence_copy(Sequence * s) {

}

Sequence * sequence_deep_copy(Sequence * s, FunctionCopy f) {
	
}

// CONTENT
void sequence_prepend(Sequence * s, T item) {
	// Preconditions
	assert(s != NULL);
	
	// Variables
	Node * n;

	// Algorithm
	// Create node
	n = malloc(sizeof(struct Node));
	if (n == NULL) {
		s->error = CC_MALLOC_ERROR;
		return NULL;
	}
	
	// Update node
	n->item = item;
	n->next = s->head;

	// Update structure
	l->head = n;
}

T sequence_get(Sequence * l, Integer i) {
	
}

T sequence_head(Sequence * s) {
	// Preconditions
	assert(s != NULL);
	assert(!sequence_is_empty(s));
	
	// Algorithm
	return s->head->item;
}

T sequence_tail(Sequence * l) {
	
}

// QUERY STATE
Integer sequence_size(Sequence * l) {
	// Preconditions
	assert(s != NULL);

	// Variables
	Integer n;

	// Algorithm
	n = 0;

}
int     sequence_is_empty(Sequence * l);

// SPECIFIC
sequence_concatenate(Sequence * l1, Sequence * l2);
sequence_sort(Sequence * l);

