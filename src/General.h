// Generic type
typedef void* T;

typedef FunctionCompare;
typedef FunctionFree;
typedef FunctionCopy;

// Integer
typedef int Integer;
typedef enum CCError CCError;
enum CCError {
	CC_SUCCESS = 0,
	CC_MALLOC_ERROR
};

// Use if Integer is a struct
/*
#define ADD(x,y) (x + y)
#define SUB(x,y) (x - y)
#define MUL(x,y) (x * y)
#define DIV(x,y) (x / y)
#define MOD(x,y) (x % y)
#define GET(x) (x)
#define SET(x,y) (x = y)

#define EQ(x,y) (x == y)
#define LE(x,y) (x <= y)
#define GE(x,y) (x >= y)
#define LT(x,y) (x < y)
#define GT(x,y) (x > y)
#define NE(x,y) (x != y)
*/
