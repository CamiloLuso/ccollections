#ifndef ITER_H
#define ITER_H

// Types
typedef struct Iter Iter;

// Functions
Iter* iter_new();
void iter_free(Iter* i);

#endif
