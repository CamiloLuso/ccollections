#include <stdlib.h>
#include <assert.h>
#include "iter.h"
#include "../General.h"

struct Iter {
	G value;
	void* next;
	void* previous;
};

Iter* iter_new() {
	// Variables
	Iter* i;

	// Algorithm
	i = malloc(sizeof(Iter));
	if (i == NULL) return NULL;

	return i;
}

void iter_free(Iter* i) {
	// Preconditions
	assert(i != NULL);
	
	// Algorithm
	free(i);
}
